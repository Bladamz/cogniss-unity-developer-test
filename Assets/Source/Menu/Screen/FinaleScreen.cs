﻿//TODO: Complete this screen
namespace Developer.Screen
{
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class FinaleScreen : Core.ScreenAbstract
    {
        Button button;
        Image background;

        private void Awake()
        {
            GameObject finPrefab = Resources.Load<GameObject>("Resources/Prefabs/Screen/FinaleScreen");
            background = this.transform.GetComponent<Image>();
            button = this.transform.Find("Button").GetComponent<Button>();
            button.onClick.AddListener(RestartApp);

#if UNITY_EDITOR
            Debug.Log("Unity Editor");
            background.color = Color.yellow;
#endif

#if UNITY_IOS
            Debug.Log("Iphone");
            background.color = Color.green;
#endif

#if UNITY_ANDROID
            Debug.Log("Android");
            background.color = Color.red;
#endif
        }

        void RestartApp()
        {
            _app.ChangeScreen<LoadingScreen>();
        }
    }



}