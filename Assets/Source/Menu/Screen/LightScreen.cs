﻿namespace Developer.Screen
{
    using UnityEngine;
    using System.Collections;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
    /// </summary>
    public sealed class LightScreen : Core.ScreenAbstract
    {
        private Developer.Component.TrafficLight lights = null;

        float elapsed;
        int testCounter = 1;
        int testTarget = 100;
        bool changeLight = true;

        private void Awake()
        {
            GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
            GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
            lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
            lights.SetState();
        }


        //TODO: According the rules, turn the lights on and off...
        //Hint: The networking functionality is inside the "Test" namespace.

        public void Start()
        {
            StartCoroutine(LightTest());
        }

        IEnumerator LightTest()
        {
            while (testCounter <= testTarget)
            {
                elapsed += Time.deltaTime;
                if (elapsed >= 3)
                {
                    elapsed = 0;
                    changeLight = true;
                }

                if (changeLight == true)
                {
                    if ((testCounter % 3 == 0) && (testCounter % 5 == 0))
                    {
                        //turn on red light
                        lights.SetState(Test.LightColor.Green);
                        print("Green" + testCounter);
                    }
                    else if (testCounter % 5 == 0)
                    {
                        //turn on yellow light	
                        lights.SetState(Test.LightColor.Yellow);
                        print("Yellow" + testCounter);
                    }
                    else if (testCounter % 3 == 0)
                    {
                        //turn on green light 
                        lights.SetState(Test.LightColor.Red);
                        print("Red" + testCounter);
                    }
                    else
                    {
                        //no lights
                        lights.SetState();
                        print("None" + testCounter);
                    }

                    changeLight = false;
                    testCounter++;
                }

                yield return null;
            }

            lights.SetState();
            _app.ChangeScreen<FinaleScreen>();

        }


    }
}
